﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{

    public GameObject shot;
    public Transform[] shotSpawns; 
    public float fireRate;
    private float nextFire;
    public float delay;
    //public AudioClip clip;

    private AudioSource audioSource;
	
	void Start () {

        audioSource = GetComponent<AudioSource>();
        InvokeRepeating("Fire",delay,fireRate);
		

	}
    void Fire ()
    {
        nextFire = Time.time + fireRate;
        foreach (var shotSpawn in shotSpawns)
        { 
        Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
        }
       /* AudioClip clip = clip[Random.Range(0, clip.Length)];
         GetComponent<AudioSource>().clip =clip;*/
      //  GetComponent<AudioSource>().Play();
    }
}
