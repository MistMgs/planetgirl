﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{

    public int startingHealth = 100;
    public int currentHealth;
    public int scoreValue = 10;  
    bool isDead;
    BoxCollider boxCollider;



    void Awake()
    {
        currentHealth = startingHealth;
    }
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void TakeDamage(int amount, Vector3 hitPoint)
    {
        if (isDead)
            return;
        currentHealth -= amount;
        if (currentHealth <= 0)
        {
            Destroy(gameObject);
        }
    }
    void Death()
    {
        isDead = true;

        boxCollider.isTrigger = true;

        DataController.Instance.gameData.Point += scoreValue;
        DataController.Instance.AddExp(scoreValue);
        Destroy(gameObject, 1f);

    }
}