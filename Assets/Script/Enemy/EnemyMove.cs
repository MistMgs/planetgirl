﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour
{
    public Boundary boundary;
    public float dodge;
    public float smoothing;


    public Vector2 startWait;
    public Vector2 maneuverTime;
    public Vector2 maneuverWait;
    public Transform playerTransform;

    private float currnetSpeed;
    private float targetManeuver;



    void Start()
    {
        currnetSpeed = GetComponent<Rigidbody>().velocity.y;
        StartCoroutine(Evade());
    }

    IEnumerator Evade()
    {
        yield return new WaitForSeconds(Random.Range(startWait.x, startWait.y));
        while (true)
        {
            targetManeuver = Random.Range(1, dodge) * -Mathf.Sign(transform.position.x);

            yield return new WaitForSeconds(Random.Range(maneuverTime.x, maneuverTime.y));
            targetManeuver = 0;
            yield return new WaitForSeconds(Random.Range(maneuverWait.x, maneuverWait.y));
        }
    }


	void FixedUpdate () 
    {
        float newManeuver = Mathf.MoveTowards(GetComponent<Rigidbody>().velocity.x, targetManeuver, smoothing * Time.deltaTime);
        GetComponent<Rigidbody>().velocity = new Vector3(newManeuver, -10f, 0.0f);
        GetComponent<Rigidbody>().position = new Vector3
        (Mathf.Clamp(GetComponent<Rigidbody>().position.x, boundary.xMin, boundary.xMax),
        (Mathf.Clamp(GetComponent<Rigidbody>().position.y, boundary.yMin, boundary.yMax))
        ,0.0f);

	}
}
	