﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour
{
    public GameObject explosion;
    public GameObject PlayerExplosion;
    public int scoreValue;
    private GameController gameController;
    PlayerHealth playerHealth;
    public int attackDamage = 10;


    void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null) ;
        {
        }
        if (gameController == null)
        {
        }

    }
    void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Boundary" || other.tag == "Enemy")
        {
            return;
        }
        if (explosion != null)
        {
            Instantiate(explosion, transform.position, transform.rotation);
        }

        if (other.tag == "Player")
        {
            Instantiate(PlayerExplosion, other.transform.position, other.transform.rotation);
            Destroy(this.gameObject);
        }
    }
}