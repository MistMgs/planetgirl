﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Boundary 
{
	public float xMin, xMax, yMin,yMax;
}

public class PlayerController : MonoBehaviour
{
	public float speed;
	public Boundary boundary;
	public GameObject shot;
	public Transform shotSpawn;
	public float fireRate;
	private float nextFire;
	public float MoveSpeed;

	public GameObject mainCamera;


	private void Start()
	{
		StartCoroutine(StartFire());
	}

	IEnumerator StartFire()
	{
		while (true)
		{
			Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
			yield return new WaitForSeconds(0.2f);
		}
	}

	void Update()
	{
		//if (Input.GetButton("Fire1") && Time.time > nextFire)
		//{
		nextFire = Time.time + fireRate;
		// GetComponent<AudioSource>().Play();
		//}
		//Debug.Log(transform.position);
		//Debug.Log(Input.mousePosition);



	}

	void FixedUpdate()
	{
		float moveHorizontal = Input.GetAxis("Horizontal");
		float moveVertical = Input.GetAxis("Vertical");

		//transform.Translate(Vector3.up * Time.deltaTime* MoveSpeed);

		Vector3 movement = new Vector3(moveHorizontal, moveVertical, 0.0f);
		GetComponent<Rigidbody>().velocity = movement * speed;

		GetComponent<Rigidbody>().position = new Vector3
		(
			Mathf.Clamp(GetComponent<Rigidbody>().position.x, boundary.xMin, boundary.xMax),
			Mathf.Clamp(GetComponent<Rigidbody>().position.y, boundary.yMin, boundary.yMax),
			0.0f
		);

		Ray ray = mainCamera.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit))
		{
			Debug.DrawLine(ray.origin, hit.point, Color.red);
			if (Input.GetMouseButton(0))
			{
				//Debug.Log(hit.point);

				Rigidbody rb = GetComponent<Rigidbody>();
				Vector3 velocity = new Vector3(
					hit.point.x - transform.position.x,
					hit.point.y - transform.position.y,
					hit.point.z - transform.position.z).normalized * MoveSpeed;
				rb.velocity = velocity;

				

				//transform.position = new Vector3(
				//	Mathf.Clamp(hit.point.x, boundary.xMin, boundary.xMax),
				//	Mathf.Clamp(hit.point.y, boundary.yMin, boundary.yMax),
				//	transform.position.z);


			}

		}
	}
}