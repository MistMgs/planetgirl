﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public GameObject[] hazards;
    public Vector3 spawnValue;
    public int hazardcount;
    public float spwanWait;
    public float startWait;
    public float waveWait;

    public Text scoreText;
 
    public GameObject restartButton;
    public Text gameOverText;

    private bool gameOver;
    private bool restart;
    private int score;


    void Start()
    {
        gameOver = false;
        restartButton.SetActive(false);
        score = 0;
        UpdateScore();
        StartCoroutine(SpawnWaves());
    }



    IEnumerator SpawnWaves()
    {

        yield return new WaitForSeconds(startWait);
        while (true)
        {
            for (int i = 0; i < hazardcount; i++)
            {
                GameObject hazard = hazards[Random.Range(0, hazards.Length)];
               
                Vector3 spawnPostion =
                new Vector3(Random.Range(-spawnValue.x, spawnValue.x), spawnValue.y, spawnValue.z);
                Quaternion spawnRoatation = Quaternion.identity;
                Instantiate(hazard, spawnPostion, spawnRoatation);
                yield return new WaitForSeconds(spwanWait);
            }
            yield return new WaitForSeconds(waveWait);

            if (gameOver)
            {
                restartButton.SetActive(true);
       
                break;
            }
        }
    }

    public void AddScore(int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }
    void UpdateScore()
    {
        scoreText.text = "Score: " + score;
    }
    public void GameOver()
    {
        gameOverText.text = "Game Over";
        gameOver = true;
    }
        public void RestartGame()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
}
