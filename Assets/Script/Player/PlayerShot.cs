﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CygniShot : MonoBehaviour
{
    public int damagePerShot = 20;
    RaycastHit shootHit;


    void Shoot()
    {
        damagePerShot = DataController.Instance.gameData.Damage;
        EnemyHealth enemyHealth = GetComponent<EnemyHealth>();
        if (enemyHealth != null)
        {
            enemyHealth.TakeDamage(damagePerShot, shootHit.point);
        }
    }
}
