﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour {

	public int startingHealth = 100;
	public int currentHealth;
    public Slider healthSlider;
	bool isDead;
	bool damaged;
    PlayerController playerController;                             

    void Awake()
    {

        playerController = GetComponent<PlayerController>();

        // Set the initial health of the player.
        currentHealth = DataController.Instance.gameData.Health;
        healthSlider.maxValue = DataController.Instance.gameData.Health;
        healthSlider.value = currentHealth;
    }

	public void Start()
	{
		currentHealth = startingHealth;
	}


	public void TakeDamage(int amount)
	{
		currentHealth -= amount;
        healthSlider.value = currentHealth;
		damaged = true;
		if (currentHealth <= 0 && !isDead)
		{
			Death();
		}
	}


	void Death()
	{
		DataController.Instance.SaveGameData();
		GameObject Player = GameObject.FindWithTag("Player");
        isDead = true;
		gameObject.SetActive(false);
        playerController.enabled = false;
		//Application.LoadLevel(Application.loadedLevel);
	}








	public void RestartLevel()
	{
		// Reload the level that is currently loaded.
		SceneManager.LoadScene(0);
	}
}